<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');
?>
<?php
/**
 * Created by PhpStorm.
 * Customer: OmarSharif
 * Date: 7/27/2017
 * Time: 10:14 AM
 */
class Brand{
    private $db;
    private $fm;

    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function brandInsert($brandName){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        if(empty($brandName)){
            $msg = "<span class='error'>Brand name must not be empty!</span>";
            return $msg;
        }
        else{
            $query = "INSERT INTO `tbl_brand` (`brandId`, `brandName`) VALUES (NULL, '$brandName');";
            $insertBrand = $this->db->insert($query);
            if($insertBrand){
                $msg = "<span class='success'>Brand Inserted Successfully!</span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Brand has not been Inserted!</span>";
                return $msg;
            }
        }
    }
    public function getAllBrand(){
        $query = "SELECT * FROM `tbl_brand` ORDER BY `brandId` DESC;";
        $result = $this->db->select($query);
        return $result;
    }
    public function getBrandById($id)
    {
        $query = "SELECT * FROM `tbl_brand` WHERE `brandId` = '$id';";
        $result = $this->db->select($query);
        return $result;
    }
    public function brandUpdate($brandName, $id){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if(empty($brandName)) {
            $msg = "<span class='error'>Brand Name must not be empty!</span>";
            return $msg;
        } else{
            $query = "UPDATE `tbl_brand` SET `brandName` = '$brandName' WHERE `brandId` ='$id';";
            $updated_row = $this->db->update($query);

            if($updated_row){
                $msg = "<span class='success'>Brand Name Updated Successfully!</span>";
                return $msg;
            } else{
                $msg = "<span class='error'>Brand Name has not been Updated!</span>";
                return $msg;
            }
        }
    }

    public function delBrandById($id){
        $query = "DELETE FROM `tbl_brand` WHERE `brandId` = '$id';";
        $deldata = $this->db->delete($query);
        if($deldata){
            $msg = "<span class='success'>Brand Name Deleted Successfully!</span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Brand Name has not been Deleted!</span>";
            return $msg;
        }
    }
}
?>


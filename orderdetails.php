<?php include 'inc/header.php';
/**
 * Created by PhpStorm.
 * Customer: OmarSharif
 * Date: 4/11/2017
 * Time: 6:38 PM
 */
?>
<?php
$login= Session::get("custlogin");
if ($login==false){
    header("Location:login.php");
}
?>
<?php
    if (isset($_GET['customerId'])){
        $id = $_GET['customerId'];
        $time = $_GET['time'];
        $price = $_GET['price'];
        $confirm = $ct->productShiftConfirm($id, $time, $price);
}
?>
<style>
    .tblone tr td{text-align: justify}
</style>
<div class="main">
    <div class="content">
        <div class="section group">
            <div class="order">
                <h2>Order Details</h2>
                <table class="tblone">
                    <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $cmrId = Session::get("cmrId");
                    $getOrder = $ct->getOrderedProduct($cmrId);
                    if($getOrder){
                        $i = 0;
                        while($result = $getOrder->fetch_assoc()){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $result['productName']; ?></td>
                                <td><img src="admin/<?php echo $result['image']; ?>" alt=""/></td>
                                <td><?php echo $result['quantity'];?></td>
                                <td>$<?php echo $result['price'];?></td>
                                <td><?php echo $fm->formatDate( $result['date']); ?></td>
                                <td>
                                    <?php
                                    if ($result['status'] == '0'){
                                        echo "Pending";
                                    }elseif($result['status'] == '1'){ ?>
                                <a href="?customerId=<?php echo $cmrId;?> & price=<?php echo $total;?> & time=<?php echo $result['date'];?>">Shifted</a>
                                   <?php }else{
                                        echo "Confirm";
                                    }
                                    ?>
                                </td>
                                <?php
                            if ($result['status'] == '2'){ ?>
                                <td><a onclick="return confirm('Are you sure to Delete?')" href="?">Remove</a></td>
                                   <?php } else{ ?>
                                <td>N/A</td>
                            <?php } ?>
                            </tr>
                        <?php }} ?>
                </table>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php include 'inc/footer.php';?>
